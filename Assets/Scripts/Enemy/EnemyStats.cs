﻿using System.Collections;
using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;

namespace Enemy
{
    public class EnemyStats : MonoBehaviour
    {
        public int Ammo = 100;
        public int Health = 100;

        public GameObject bloodEffect;

        public SpriteShapeRenderer body;
        
        public void takeDamage(int damage)
        {
            StartCoroutine(Flash());
            Health -= damage;
            
            if (Health < 0)
            {
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }

        IEnumerator Flash()
        {
            body.color = Color.white;

            yield return new WaitForSeconds(0.05f);

            body.color = Color.blue;
        }
    }
}