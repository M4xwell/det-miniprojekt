using System;
using Menu;
using Pathfinding;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Enemy
{
    public class EnemyDefuse : MonoBehaviour
    {
        public GameObject defuseCounter;
        public GameObject defusingText;
        private GameObject bomb;

        private float currentTime = 0f;
        private float startingTime = 10;

        private void Start()
        {
            defuseCounter.SetActive(false);
            defusingText.SetActive(false);
            currentTime = startingTime;
        }

        private void FixedUpdate()
        {
            GameObject[] bombs = GameObject.FindGameObjectsWithTag("Bomb");
            if (bombs.Length > 0)
            {
                bomb = bombs[0];
                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

                foreach (GameObject enemy in enemies)
                {
                    enemy.GetComponent<AIDestinationSetter>().target = bomb.transform;
                }
            }
        }

        private void Update()
        {
            if (bomb != null && Vector2.Distance(transform.position, bomb.transform.position) < 0.5f)
            {
                defuseCounter.SetActive(true);
                defusingText.SetActive(true);

                Text text = defuseCounter.GetComponent<Text>();

                currentTime -= Time.deltaTime;
                text.text = currentTime.ToString();

                if (currentTime <= 0)
                {
                    EndGame.winner = Winner.Bots;
                    SceneManager.LoadScene("EndGame");
                }
            }
            else
            {
                defuseCounter.SetActive(false);
                defusingText.SetActive(false);
                
                currentTime = startingTime;
            }
        }
    }
}