﻿using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyTerroristMovement : MonoBehaviour
    {
        public float speed = 10f;

        public float stoppingDistance = 10f;

        public Transform target;

        public Rigidbody2D body;

        public Transform bombSite;

        private bool bombIsPlaced;

        public GameObject bomb;

        private void Start()
        {
            if (bomb == null)
            {
                bombIsPlaced = true;
            }
        }

        private void Update()
        {
            if (bombIsPlaced)
            {
                MoveToPlayer();
            }
            else
            {
                MoveToBombSite();
            }
        }

        private void MoveToBombSite()
        {
            Vector3 bombSitePosition = bombSite.position;
            if (Vector2.Distance(transform.position, bombSitePosition) > 0.1f)
            {
                transform.position = Vector2.MoveTowards(transform.position, bombSitePosition, speed * Time.deltaTime);
                rotateBody(new Vector2(bombSitePosition.x, bombSitePosition.y));
            }
            else
            {
                Instantiate(bomb, gameObject.transform.position, gameObject.transform.rotation);
                bombIsPlaced = true;
            }
        }

        private void MoveToPlayer()
        {
            if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                rotateBody(new Vector2(target.position.x, target.position.y));
            }
        }

        private void rotateBody(Vector2 targetPos)
        {
            var playerPos = new Vector2(target.position.x, target.position.y);
            var lookDir = targetPos - body.position;

            var angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            body.rotation = angle;
        }
    }
}