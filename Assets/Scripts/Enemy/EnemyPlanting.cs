using System;
using Menu;
using Pathfinding;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Enemy
{
    public class EnemyPlanting : MonoBehaviour
    {
        private GameObject bomb;

        private float currentTime = 0f;
        private float startingTime = 5f;
        
        public Transform bombSite;


        private void Start()
        {
            currentTime = startingTime;

            if (bomb != null)
            {
                gameObject.GetComponent<AIDestinationSetter>().target = bomb.transform;
            }
        }

        private void Update()
        {
            if (Vector2.Distance(transform.position, bomb.transform.position) < 0.5f)
            {
                currentTime -= Time.deltaTime;
            }
            else
            {
                currentTime = startingTime;
            }
        }
    }
}