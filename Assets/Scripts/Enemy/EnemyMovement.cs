﻿using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        public float speed = 10f;

        public float stoppingDistance = 10f;

        public Transform target;

        public Rigidbody2D body;

        public PlaceBomb placeBomb;

        private GameObject bomb;

        private void Update()
        {
            if (placeBomb.bombPlaced)
            {
                MoveToBomb();
            }
            else
            {
                MoveToPlayer();
            }
        }

        private void FixedUpdate()
        {
            GameObject[] bombs = GameObject.FindGameObjectsWithTag("Bomb");
            if (bombs.Length > 0)
            {
                bomb = bombs[0];
            }
        }

        private void MoveToBomb()
        {
            Vector3 bombPosition = bomb.transform.position;

            if (Vector2.Distance(transform.position, bombPosition) > 0.1f)
            {
                transform.position = Vector2.MoveTowards(transform.position, bombPosition, speed * Time.deltaTime);
                rotateBody(new Vector2(bombPosition.x, bombPosition.y));
            }
        }

        private void MoveToPlayer()
        {
            if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                rotateBody(new Vector2(target.position.x, target.position.y));
            }
        }

        private void rotateBody(Vector2 targetPos)
        {
            var playerPos = new Vector2(target.position.x, target.position.y);
            var lookDir = targetPos - body.position;

            var angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            body.rotation = angle;
        }
    }
}