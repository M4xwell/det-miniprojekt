﻿using UnityEngine;

namespace Enemy
{
    public class EnemyShooting : MonoBehaviour
    {
        private float timeBtwShots;

        public float startTimeBtwShots = 2;

        public GameObject bulletPrefab;

        public float bulletForce = 30f;

        public Transform firePoint;

        private void Start()
        {
            timeBtwShots = startTimeBtwShots;
        }

        private void Update()
        {
            if (timeBtwShots <= 0)
            {
                GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);

                timeBtwShots = startTimeBtwShots;
            }
            else
            {
                timeBtwShots -= Time.deltaTime;
            }
        }
    }
}