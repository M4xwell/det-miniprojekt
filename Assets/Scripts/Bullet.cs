﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Enemy;
using Player;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int damage = 20;
    public GameObject bloodEffect;
    public GameObject bulletEffect;

    private void OnCollisionEnter2D(Collision2D other)
    {
        var collisionObj = other.gameObject;
        if (collisionObj.CompareTag("Level"))
        {
            Instantiate(bulletEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        if (collisionObj.CompareTag("Enemy"))
        {
            Instantiate(bloodEffect, transform.position, Quaternion.identity);

            var enemyStats = collisionObj.GetComponent<EnemyStats>();
            enemyStats.takeDamage(damage);

            Destroy(gameObject);
        }

        if (collisionObj.CompareTag("Player"))
        {
            var playerStats = collisionObj.GetComponent<PlayerStats>();
            playerStats.takeDamage(damage);

            Destroy(gameObject);
        }
    }
}