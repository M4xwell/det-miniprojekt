﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Menu
{
    public class CountDownTimer : MonoBehaviour
    {
        public float currentTime = 0f;
        private float startingTime = 120f;

        public Text countDownText;

        public PlaceBomb placeBomb;
        
        private void Start()
        {
            currentTime = startingTime;
        }
        
        private void Update()
        {
            currentTime -= Time.deltaTime;
            countDownText.text = currentTime.ToString("0");

            if (currentTime <= 10) countDownText.color = Color.red;

            if (currentTime <= 0)
            {
                currentTime = 0;
                EndGame.winner = Winner.Bots;
                SceneManager.LoadScene("EndGame");
            }
        }
    }
}