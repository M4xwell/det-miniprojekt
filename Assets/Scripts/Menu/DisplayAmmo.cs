﻿using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class DisplayAmmo : MonoBehaviour
    {
        public PlayerStats playerStats;

        public Text ammoCount;

        private void Update()
        {
            ammoCount.text = playerStats.Ammo.ToString();
        }
    }
}