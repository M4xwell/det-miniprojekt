﻿using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class HealthBar : MonoBehaviour
    {
        public PlayerStats playerStats;


        public Slider playerSlider;


        private void Start()
        {
            playerSlider.maxValue = 100;
            playerSlider.value = playerStats.Health;
        }

        public void SetHealth(int health)
        {
            playerSlider.value = health;
        }
    }
}