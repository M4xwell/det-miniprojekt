﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class SelectMenu : MonoBehaviour
    {
        public void Back()
        {
            Debug.Log("Back");
            SceneManager.LoadScene("MainMenu");
        }

        public void SelectT()
        {
            SceneManager.LoadScene("TerroristScene");
        }

        public void SelectCT()
        {
            SceneManager.LoadScene("CounterTerroristScene");
        }
    }
}