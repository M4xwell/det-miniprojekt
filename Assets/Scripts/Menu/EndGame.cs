﻿using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public static Winner winner;

    public Text winnerText;

    private void Start()
    {
        winnerText.text = winner + " won!";
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}