﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MainMenu : MonoBehaviour
    {
        public void PlayGame()
        {
            SceneManager.LoadScene("LevelSelect");
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}