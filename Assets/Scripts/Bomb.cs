using System;
using System.Threading;
using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class Bomb : MonoBehaviour
    {
        public SpriteShapeRenderer sprite;
        
        public float timer = 40f;

        private bool switchColor = false;
        
        private void Start()
        {
            InvokeRepeating(nameof(setColor), 0, 1F);
            CountDownTimer countDownTimer = new CountDownTimer();
            countDownTimer.currentTime = timer;
        }

        private void FixedUpdate()
        {
            Countdown();
        }

        private void Countdown()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                EndGame.winner = Winner.Player;
                SceneManager.LoadScene("EndGame");
            }
        }

        private void setColor()
        {
            if (switchColor)
            {
                sprite.color = Color.black;
                switchColor = false;
                return;
            }

            sprite.color = Color.white;
            switchColor = true;
        }
    }
}