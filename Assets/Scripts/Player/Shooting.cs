﻿using UnityEngine;

namespace Player
{
    public class Shooting : MonoBehaviour
    {
        public Transform firePoint;

        public GameObject bulletPrefab;

        public float bulletForce = 30f;

        public PlayerStats playerStats;

        private void Update()
        {
            if(!Pause.isGamePaused) 
            { 
                if (Input.GetButtonDown("Fire1") && playerStats.Ammo > 0)
                {
                    GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                    Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                    rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
                    playerStats.Ammo--;
                }
            }
        }
    }
}