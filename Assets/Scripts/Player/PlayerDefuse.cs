using System;
using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Enemy
{
    public class PlayerDefuse : MonoBehaviour
    {
        public GameObject defuseCounter;
        public GameObject defusingText;
        private GameObject bomb;

        private float currentTime = 0f;
        private float startingTime = 5;

        private void Start()
        {
            defuseCounter.SetActive(false);
            defusingText.SetActive(false);
            currentTime = startingTime;
        }

        private void FixedUpdate()
        {
            bomb = GameObject.FindGameObjectsWithTag("Bomb")[0];
        }

        private void Update()
        {
            if (Vector2.Distance(transform.position, bomb.transform.position) < 2f)
            {
                if (Input.GetKey(KeyCode.Space))
                {
                    defuseCounter.SetActive(true);
                    defusingText.SetActive(true);

                    Text text = defuseCounter.GetComponent<Text>();

                    currentTime -= Time.deltaTime;
                    text.text = currentTime.ToString();

                    if (currentTime <= 0)
                    {
                        EndGame.winner = Winner.Player;
                        SceneManager.LoadScene("EndGame");
                    }   
                }
            }
            else
            {
                defuseCounter.SetActive(false);
                defusingText.SetActive(false);
                
                currentTime = startingTime;
            }
        }
    }
}