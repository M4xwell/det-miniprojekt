﻿using UnityEngine;

public class PlaceBomb : MonoBehaviour
{
    public GameObject bomb;

    public bool bombPlaced = false;

    private bool atBombSite = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !bombPlaced && atBombSite)
        {
            bombPlaced = true;
            Instantiate(bomb, transform.position, transform.rotation);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bombsite"))
        {
            atBombSite = true;
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bombsite"))
        {
            atBombSite = false;
        }
    }
}