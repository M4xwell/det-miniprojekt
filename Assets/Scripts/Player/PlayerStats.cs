﻿using System.Collections;
using Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;

namespace Player
{
    public class PlayerStats : MonoBehaviour
    {
        public int Ammo = 100;
        public int Health = 100;
        public int Speed = 10;

        public HealthBar healthBar;

        public GameObject bloodEffect;
        
        public SpriteShapeRenderer body;


        private void FixedUpdate()
        {
            healthBar.SetHealth(Health);
            
            if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
            {
                EndGame.winner = Winner.Player;
                SceneManager.LoadScene("EndGame");
            }
        }
        
        public void takeDamage(int damage)
        {
            Instantiate(bloodEffect, transform.position, Quaternion.identity);
            StartCoroutine(Flash());
            Health -= damage;
            
            if (Health < 0)
            {
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                Instantiate(bloodEffect, transform.position, Quaternion.identity);
                
                Destroy(gameObject);
                EndGame.winner = Winner.Bots;
                SceneManager.LoadScene("EndGame");
            }
        }
        
        IEnumerator Flash()
        {
            body.color = Color.white;

            yield return new WaitForSeconds(0.05f);

            body.color = Color.green;
        }
    }
}